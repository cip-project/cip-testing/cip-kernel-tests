/*
 * Project: RZG_IT_CIP_BSP
 * Test ID: test_case
 * Feature: Checking system call open of SDHI
 * Sequence: open; close
 * Testing level: system call
 * Test-case type: Normal
 * Expected result: OK
 * Name: main.c
 * Author: RVC/TinLe (tin.le.wh@rvc.renesas.com)
 * Version: v00r01
 * Copyright (C) 2019  Renesas Electronics Corporation
 * Target board: G1M_G1E_G1C_G1N_G1H
 * Details_description: External device: Boot from SD Card 4GB or over. Condition: Call open with device: SD_slot0; mode: O_RDONLY. Expected result = OK
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <libgen.h>
#include <limits.h>
#include <ctype.h>
#include <errno.h>
#include <stdint.h>
#include <assert.h>
#include <linux/fs.h>
#include "mmc_user.h"
#include <linux/mmc/ioctl.h>
#include "get_device.h"

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int	result = -1;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */

	//Get mmc device from file ./board_config.txt
	char sd_dev[12] = "/dev/";
	char *sd_dev_get;
	sd_dev_get = get_ethernet_device("SD0_DEV");
	strcat(sd_dev, sd_dev_get);

	/* Call API or system call follow describe in PCL */
	result = open(sd_dev, O_RDONLY);

	/* Check return value of sequence */
	if (result >= 0) {
		printf("OK\n");
		close(result);
	} else {
		printf("NG\n");
	}
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

