/*
 * Project: RZG_IT_CIP_BSP
 * Test ID: test_case
 * Feature: Checking system call read of SDHI
 * Sequence: open; read; close
 * Testing level: system call
 * Test-case type: Normal
 * Expected result: OK
 * Name: main.c
 * Author: RVC/TinLe (tin.le.wh@rvc.renesas.com)
 * Version: v00r01
 * Copyright (C) 2019  Renesas Electronics Corporation
 * Target board: G1M_G1E_G1C_G1N_G1H
 * Details_description: External device: Boot from SD Card 4GB or over. Condition: Call read with device: SD_slot0; mode: O_RDWR; then call ioclt with opcode MMC_READ_MULTIPLE_BLOCK; arg = 0x2EE000; blocks = 2. Expected result = OK
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <libgen.h>
#include <limits.h>
#include <ctype.h>
#include <errno.h>
#include <stdint.h>
#include <assert.h>
#include <linux/fs.h>
#include "mmc_user.h"
#include <linux/mmc/ioctl.h>
#include "get_device.h"

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int	result = -1;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	struct mmc_ioc_cmd	cmd;
	__u8 buf[2048];

	//Get mmc device from file ./board_config.txt
	char sd_dev[12] = "/dev/";
	char *sd_dev_get;
	sd_dev_get = get_ethernet_device("SD0_DEV");
	strcat(sd_dev, sd_dev_get);
	int block_num = get_block_number("SD0_DEV");
	int block_size = get_block_size("SD0_DEV");

	/* Call API or system call follow describe in PCL */
	fd = open(sd_dev, O_RDWR);

	memset(&buf, 0, sizeof(buf));
	memset(&cmd, 0, sizeof(cmd));
	cmd.write_flag	= 0;
	cmd.opcode	= MMC_READ_MULTIPLE_BLOCK;
	cmd.arg		= block_num;	//4 highest block
	cmd.flags	= MMC_RSP_SPI_R1 | MMC_RSP_R1 | MMC_CMD_ADTC;
	cmd.blksz	= block_size;	//Block size
	cmd.blocks	= 2;
	mmc_ioc_cmd_set_data(cmd, buf);

	result = ioctl(fd, MMC_IOC_CMD, &cmd);

	/* Check return value of sequence */
	switch(result) {
	case 0:
		printf ("OK\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unknown_result\n");
	};
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

