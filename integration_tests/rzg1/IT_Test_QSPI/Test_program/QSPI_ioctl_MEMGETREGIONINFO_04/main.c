/*
 * Project: RZG_IT_CIP_BSP
 * Test ID: test_case
 * Feature: Checking ioctl_MEMGETREGIONINFO system call
 * Sequence: Open();ioctl_MEMGETREGIONINFO()
 * Testing level: system call
 * Test-case type: Abnormal
 * Expected result: NG
 * Name: main.c
 * Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
 * Version: v00r01
 * Copyright (C) 2019  Renesas Electronics Corporation
 * Target board: G1M_G1E_G1C_G1N_G1H
 * Details_description: Condition: Call ioctl_MEMGETREGIONINFO with device /dev/mtd0ro; mode O_RDONLY. Expected result = NG
 */
#include <stdio.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <mtd/mtd-user.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main() {
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int	result, fd;
	mtd_info_t	device_info;
	erase_info_t	ei;
	struct region_info_user	region;

	fd = open("/dev/mtd0ro", O_RDONLY);

	result = ioctl(fd, MEMGETREGIONINFO , &region);

	switch(result) {
	case 0:
		printf ("OK\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unkonw_result\n");
	};
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}
