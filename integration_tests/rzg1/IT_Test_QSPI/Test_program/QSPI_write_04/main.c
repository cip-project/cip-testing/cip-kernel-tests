/*
 * Project: RZG_IT_CIP_BSP
 * Test ID: test_case
 * Feature: Checking write system call
 * Sequence: Open();ioctl_MEMGETINFO();ioctl_MEMERASE();write()
 * Testing level: system call
 * Test-case type: Abnormal
 * Expected result: NG
 * Name: main.c
 * Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
 * Version: v00r01
 * Copyright (C) 2019  Renesas Electronics Corporation
 * Target board: G1M_G1E_G1C_G1N_G1H
 * Details_description: Condition: Call write with device /dev/mtd0ro; mode O_RDONLY. Expected result = NG
 */
#include <stdio.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <mtd/mtd-user.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main() {
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int	result, fd;
	mtd_info_t	device_info;
	erase_info_t	ei;
	unsigned char	writedata[1] = { 0xFA};
	unsigned char	readdata[1] = {0x00};
	__u32 ei_start;

	fd = open("/dev/mtd0ro", O_RDONLY);

	ioctl(fd, MEMGETINFO, &device_info);	//Get Flash information

	ei.length = device_info.erasesize;
	ei_start = device_info.size - 2*device_info.erasesize;
	ei.start = ei_start;
	ioctl(fd, MEMERASE, &ei);	//Erase flash device

	lseek(fd, ei_start, SEEK_SET);
	result = write(fd, writedata, sizeof(writedata));	//Write data into flash device

	switch(result) {
	case sizeof(writedata):		//Return of write system call = size of data

		lseek(fd, ei_start, SEEK_SET);
		read(fd, readdata, sizeof(readdata));	//Read data from flash device

		if(writedata[0] == readdata[0])	//Compare data
		{
			printf ("OK\n");
		}
		else
		{
			printf ("NG_diff_write_read_data\n");
			//printf("readdata_value = 0x%02x\n",(unsigned int)readdata[0]);
		}
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unkonw_result\n");
	};
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}
