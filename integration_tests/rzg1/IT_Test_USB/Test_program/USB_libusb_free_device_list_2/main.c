/*
 * Project: RZG_IT_CIP_BSP
 * Test ID: test_case
 * Feature: Checking libusb_free_device_list system call
 * Sequence: libusb_init();libusb_get_device_list();libusb_free_device_list();libusb_get_bus_number()
 * Testing level: system call
 * Test-case type: Abnormal
 * Expected result: NG_SF
 * Name: main.c
 * Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
 * Version: v00r01
 * Copyright (C) 2019  Renesas Electronics Corporation
 * Target board: G1M_G1E_G1C_G1N_G1H
 * Details_description: External device: USB Storage. Condition: Call API libusb_free_device_list of usblib library with context NULL. Expected result = NG_SF
 */
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <libusb.h>
#include <sys/types.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main(int argc, char *argv[])
{
	int result = -1;
	struct libusb_context *ctx = NULL;
	struct libusb_device **dev_list;

	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	//Main checking item
	libusb_init(&ctx);
	libusb_get_device_list(ctx, &dev_list);
	libusb_free_device_list(dev_list, 0);
	libusb_device *dev = dev_list[0];
	uint8_t bus_num = libusb_get_bus_number(dev);	//Test effect of libusb_free_device_list
	//printf("Bus %u\n", bus_num);			//Expected: segmentation fault due to dev=NULL

	result = bus_num;
	libusb_exit(ctx);

	if( result >= 0 ) {
		printf ("OK\n");
	}
	else {
		printf ("NG\n");
	}

	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}
