/*
 * Project: RZG_IT_CIP_BSP
 * Test ID: test_case
 * Feature: Checking API close of Serial Debug driver
 * Sequence: open() -> configuration sequence -> write() -> close()
 * Testing level: API user lib
 * Test-case type: Normal
 * Expected result: OK
 * Name: main.c
 * Author: RVC/HungDong (hung.dong.xd@rvc.renesas.com)
 * Version: v00r01
 * Copyright (C) 2019  Renesas Electronics Corporation
 * Target board: G1H_G1M_G1N_G1E
 * Details_description: Condition: Call close finally after configuring port operation O_RDWR + B57600 and calling write with NULL buffer. Expected result = OK
 */

#include <stdio.h>
#include <fcntl.h>	/* File Control Definitions				*/
#include <termios.h>	/* POSIX Terminal Control Definitions			*/
#include <unistd.h>	/* UNIX Standard Definitions				*/
#include <errno.h>	/* ERROR Number Definitions				*/
#include <sys/ioctl.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

#define _POSIX_SOURCE	1	/* POSIX compliant source */
#define FALSE		0
#define TRUE		1

/* Declare global variable */
int result = -1;

int main (void)
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd, fd_restore;
	struct termios SerialPortSettings, SerialPortSettings_restore;	/* Create the structure */
	char write_buffer[] = "R";		/* Buffer containing characters to write into port */

	/* Initialize variable and assign value for variable */

	/* Call API or system call follow describe in PCL */
	fd = open("/dev/ttySC0", O_RDWR | O_NOCTTY );	/* ttyUSB0 is the FT232 based USB2SERIAL Converter */
	/* O_RDWR, O_RDONLY, O_WRONLY - Read/Write access to serial port */
	/* O_NOCTTY - No terminal will control the process */
	/* Open in blocking mode,read will wait */

	/*------------------ Setting the Attributes of the serial port using termios structure ---------------- */
	tcgetattr(fd, &SerialPortSettings);				/* Get the current attributes of the Serial port */
	tcgetattr(fd, &SerialPortSettings_restore);		/* Backup the current attributes of the Serial port */

	/* Setting the Baud rate */
	cfsetispeed(&SerialPortSettings, B57600);	/* Set Read Speed as B57600 */
	cfsetospeed(&SerialPortSettings, B57600);	/* Set Write Speed as B57600 */

	/* Setting 8N1 Control Mode */
	SerialPortSettings.c_cflag &= ~PARENB;			/* Disables the Parity Enable bit(PARENB), so No Parity */
	SerialPortSettings.c_cflag &= ~CSTOPB;			/* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit*/
	SerialPortSettings.c_cflag &= ~CSIZE;			/* Clears the mask for setting the data size			*/
	SerialPortSettings.c_cflag |= CS8;				/* Set the data bits = 8								*/
	SerialPortSettings.c_cflag &= ~CRTSCTS;			/* No Hardware flow Control								*/
	SerialPortSettings.c_cflag |= CREAD | CLOCAL;	/* Enable receiver,Ignore Modem Control lines			*/

	/* Setting Input Mode */
	SerialPortSettings.c_iflag &= ~(IXON | IXOFF | IXANY);			/* Disable XON/XOFF flow control both i/p and o/p */

	/* Setting Local Mode */
	SerialPortSettings.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);	/* Non Cannonical mode, No echo, No signals */

	/* Setting Output Mode */
	SerialPortSettings.c_oflag &= ~OPOST;			/* No Output Post-processing */

	/* Setting Time-out */
	SerialPortSettings.c_cc[VMIN] = 1;				/* Blocking read until 10 character arrives */
	SerialPortSettings.c_cc[VTIME] = 0;				/* Wait indefinetly (timer unused) */

	/* Discards old read/write data on the modem line and activate the settings for the port */
	tcflush(fd, TCIOFLUSH);
	tcsetattr(fd, TCSANOW, &SerialPortSettings);

	/*------------ Write data to serial port ----------------------*/
	write(fd, NULL, 1);/* use write() to send data to port */
	/* "fd" - file descriptor pointing to the opened serial port */

	/* Close tty device */
	result = close(fd);

	/* Check return value of sequence */
	switch(result) {
	case 0:
		printf ("OK\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unknown_result\n");
	};

	/* Restore old port settings */
	fd_restore = open("/dev/ttySC0", O_RDWR | O_NOCTTY );

	tcsetattr(fd_restore, TCSANOW, &SerialPortSettings_restore);
	close(fd_restore);

	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

