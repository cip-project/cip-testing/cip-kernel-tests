/*
 * Project: RZG_IT_CIP_BSP
 * Test ID: test_case
 * Feature: Checking API close of Serial Debug driver
 * Sequence: open() -> configuration sequence -> close()
 * Testing level: API user lib
 * Test-case type: Normal
 * Expected result: OK
 * Name: main.c
 * Author: RVC/HungDong (hung.dong.xd@rvc.renesas.com)
 * Version: v00r01
 * Copyright (C) 2019  Renesas Electronics Corporation
 * Target board: G1H_G1M_G1N_G1E
 * Details_description: Condition: Call close with with valid open mode O_RDWR after setting bit rate B9600. Expected result = OK
 */

#include <stdio.h>
#include <fcntl.h>	/* File Control Definitions				*/
#include <termios.h>	/* POSIX Terminal Control Definitions			*/
#include <unistd.h>	/* UNIX Standard Definitions				*/
#include <errno.h>	/* ERROR Number Definitions				*/
#include <sys/ioctl.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

#define _POSIX_SOURCE	1	/* POSIX compliant source */
#define FALSE		0
#define TRUE		1

/* Declare global variable */
int result = -1;

int main (void)
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd, fd_restore;
	struct termios SerialPortSettings, SerialPortSettings_restore; /* Create the structure */
	/* Initialize variable and assign value for variable */

	/* Call API or system call follow describe in PCL */
	fd = open("/dev/ttySC0", O_RDWR | O_NOCTTY );	/* ttyUSB0 is the FT232 based USB2SERIAL Converter */
	/* O_RDWR, O_RDONLY, O_WRONLY - Read/Write access to serial port */
	/* O_NOCTTY - No terminal will control the process */
	/* Open in blocking mode,read will wait */

	/*------------------ Setting the Attributes of the serial port using termios structure ---------------- */
	tcgetattr(fd, &SerialPortSettings);				/* Get the current attributes of the Serial port */
	tcgetattr(fd, &SerialPortSettings_restore);		/* Backup the current attributes of the Serial port */

	/* Setting the Baud rate */
	cfsetispeed(&SerialPortSettings, B9600);	/* Set Read Speed as B9600 */
	cfsetospeed(&SerialPortSettings, B9600);	/* Set Write Speed as B9600 */

	/* Discards old read/write data on the modem line and activate the settings for the port */
	tcflush(fd, TCIOFLUSH);
	tcsetattr(fd, TCSANOW, &SerialPortSettings);

	/* Close tty device */
	result = close(fd);

	/* Check return value of sequence */
	switch(result) {
	case 0:
		printf ("OK\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unknown_result\n");
	};

	/* Restore old port settings */
	fd_restore = open("/dev/ttySC0", O_RDWR | O_NOCTTY );
	tcsetattr(fd_restore, TCSANOW, &SerialPortSettings_restore);
	close(fd_restore);

	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

