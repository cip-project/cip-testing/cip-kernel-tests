/*
 * Project: RZG_IT_CIP_BSP
 * Test ID: test_case
 * Feature: Checking system call erase of MMC
 * Sequence: open; write; read; erase_group_start; erase_group_end; erase; read; close
 * Testing level: system call
 * Test-case type: Normal
 * Expected result: OK
 * Name: main.c
 * Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
 * Version: v00r01
 * Copyright (C) 2019  Renesas Electronics Corporation
 * Target board: G1M_G1E_G1C_G1N_G1H
 * Details_description: 0
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <libgen.h>
#include <limits.h>
#include <ctype.h>
#include <errno.h>
#include <stdint.h>
#include <assert.h>
#include <linux/fs.h>
#include <include/linux/mmc/mmc.h>
#include <linux/mmc/ioctl.h>
#include "get_device.h"

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int	result = -1;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd, i;
	int check_blocks_erased = 0;
	struct mmc_ioc_cmd	cmd;
	__u8 wbuf[1024];
	__u8 rbuf[1024];
	__u8 cbuf[1024];

	//Get mmc device from file ./board_config.txt
	char mmc_dev[12] = "/dev/";
	char *mmc_dev_get;
	mmc_dev_get = get_ethernet_device("MMC_DEV");
	strcat(mmc_dev, mmc_dev_get);
	int block_num = get_block_number("MMC_DEV");
	int block_size = get_block_size("MMC_DEV");

	/* Call API or system call follow describe in PCL */
	fd = open(mmc_dev, O_RDONLY);

	/* Write multiple blocks */
	memset(&wbuf, 1, sizeof(wbuf));
	memset(&cmd, 0, sizeof(cmd));
	cmd.write_flag	= 1;
	cmd.opcode	= MMC_WRITE_MULTIPLE_BLOCK;
	cmd.arg		= block_num;	//4 highest block
	cmd.flags	= MMC_RSP_SPI_R1 | MMC_RSP_R1 | MMC_CMD_ADTC;
	cmd.blksz	= block_size;   //Block size
	cmd.blocks	= 2;
	mmc_ioc_cmd_set_data(cmd, &wbuf);
	ioctl(fd, MMC_IOC_CMD, &cmd);
	sleep(1);

	/* Read multiple blocks */
	memset(rbuf, 0, sizeof(rbuf));
	memset(&cmd, 0, sizeof(cmd));
	cmd.write_flag	= 0;
	cmd.opcode	= MMC_READ_MULTIPLE_BLOCK;
	cmd.arg		= block_num;	//4 highest block
	cmd.flags	= MMC_RSP_SPI_R1 | MMC_RSP_R1 | MMC_CMD_ADTC;
	cmd.blksz	= block_size;   //Block size
	cmd.blocks	= 2;
	mmc_ioc_cmd_set_data(cmd, &rbuf);
	ioctl(fd, MMC_IOC_CMD, &cmd);
	sleep(1);

	/* Set erase group start */
	memset(&cmd, 0, sizeof(cmd));
	cmd.write_flag	= 1;
	cmd.opcode	= MMC_ERASE_GROUP_START;
	cmd.arg		= block_num;	//4 highest block
	cmd.flags	= MMC_RSP_SPI_R1 | MMC_RSP_R1 | MMC_CMD_AC;
	ioctl(fd, MMC_IOC_CMD, &cmd);
	sleep(1);

	/* Set erase group end */
	memset(&cmd, 0, sizeof(cmd));
	cmd.write_flag	= 1;
	cmd.opcode	= MMC_ERASE_GROUP_END;
	cmd.arg		= block_num+1;	//4 highest block
	cmd.flags	= MMC_RSP_SPI_R1 | MMC_RSP_R1 | MMC_CMD_AC;
	ioctl(fd, MMC_IOC_CMD, &cmd);
	sleep(1);

	/* Erase group */
	memset(&cmd, 0, sizeof(cmd));
	cmd.write_flag	= 0;
	cmd.opcode	= MMC_ERASE;
	cmd.flags	= MMC_RSP_SPI_R1B | MMC_RSP_R1B | MMC_CMD_AC;
	result = ioctl(fd, MMC_IOC_CMD, &cmd);
	sleep(1);
	if (result != 0)
		goto exit;

	/* Read multiple blocks */
	memset(&cbuf, 0, sizeof(cbuf));
	memset(&cmd, 0, sizeof(cmd));
	cmd.write_flag	= 0;
	cmd.opcode	= MMC_READ_MULTIPLE_BLOCK;
	cmd.arg		= block_num;	//4 highest block
	cmd.flags	= MMC_RSP_SPI_R1 | MMC_RSP_R1 | MMC_CMD_ADTC;
	cmd.blksz	= block_size;   //Block size
	cmd.blocks	= 2;
	mmc_ioc_cmd_set_data(cmd, &cbuf);
	ioctl(fd, MMC_IOC_CMD, &cmd);
	sleep(1);

exit:
	/* Check return value of sequence */
	switch(result) {
	case 0:
		for (i = 0; i < block_size*2 ; i++) {
			if (cbuf[i] != 0) {
				check_blocks_erased = -1;
				break;
			}
		}
		if (check_blocks_erased == 0)
			printf ("OK\n");
		else
			printf ("NG_not_erased\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unknown_result\n");
	};
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

