# cip-kernel-tests

Tests developed by CIP Members and contributors

## test_cases

LAVA test definitions for some CIP specific test cases.

**NOTE: In order to run any jobs that require an internet connection to install
any test dependencies, you need to run the setup\_internet.yaml test before any
tests that do require the internet.**

## integration_tests

### Renesas RZ/G1

Integration tests for RZ/G1 can be found in ./integration_tests/rzg1/

To run these tests, you can use the following job definition as a template:
./example_job_definitions/rzg1_integration_tests.yaml

## example_job_definitions

These are a number of different LAVA job definitions which you can use as a
template.

To run a job in CIP LAVA:
1) Login to https://lava.ciplatform.org
2) Scheduler -> Submit
3) Copy and paste your definition into the submit job window, validate and submit

If you would like to receive an email notification about a job status, add the
following section at the top of the job definition:

```
notify:
  criteria:
    status: finished
  recipients:
  - to:
     method: email
     user: <your_username>
```
